process_the_loop = [true, false].sample

if process_the_loop == TRUE
  puts "The loop was processed!"
else
  process_the_loop == FALSE
  puts "The loop was not processed!"
end

puts "--------------------------------"

process_the_loop = [true, false].sample

if process_the_loop
  loop do
    puts "The loop was processed!"
    break
  end
else
  puts "The loop wasn't processed!"
end
require 'Packetfu'

include Packetfu

i = ICMPPacket.new(:config=>Utils.whoami?)

i.ip_daddr = "10.126.0.250"

i.icmp_type = 8

i.icmp_code = 0

i.payload = "some data...."

i.recalc

i.to_w

t.tcp_dport = 200


# sniffing 


cap = PacketFu::Capture.new(:iface => 'eth0', :promisc => true)
cap.start
sleep 10
cap.save
first_packet = cap.array[0]

# Tcpdump-like use
cap = PacketFu::Capture.new(:start => true)
cap.show_live(:save => true, :filter => 'tcp and not port 22')

cap.array

p = Packet.parse(cap.array[0])

p.class
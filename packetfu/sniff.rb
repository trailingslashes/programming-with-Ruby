require 'packetfu'
include PacketFu

def sniff(iface)
	cap = Capture.new(:iface=>iface,:promisc=>true,:start=>true,:filter=>'tcp dst port http')
	cap.stream.each do |p|
	  pkt = Packet.parse p
	  puts pkt.payload
  end
end

sniff(ARGV[0])
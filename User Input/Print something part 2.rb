# loop do
#   puts 'Do you want me to print something? (y/n)'
#   answer = gets.chomp.downcase
#   if answer == 'y' && answer == 'n'
#     break if %w(y n).include?(answer)
#   end
# end
# puts 'something' if choice == 'y'

# choice = nil
# loop do
#   puts '>> Do you want me to print something? (y/n)'
#   choice = gets.chomp.downcase
#   break if %w(y n).include?(choice)
#   puts '>> Invalid input! Please enter y or n'
# end
# puts 'something' if choice == 'y'
choice = nil
loop do
  puts '>> Do you want me to print something? (y/n)'
  choice = gets.chomp.downcase
  break if %w(y n).include?(choice)
  puts ">> Invalid input! Please enter y or n"
end
if choice == 'y'
  puts 'something'
end
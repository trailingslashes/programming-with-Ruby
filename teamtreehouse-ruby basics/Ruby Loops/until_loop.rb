# runs as long as the condition is false, breaks when true

answer = ""
until answer == "no" do # you can remove the do when using while,until loops
  print "Do you want this loop to continue (y/n)"
  answer = gets.chomp
end


number = 75
if 75 > 50
  puts "75 > 50"
end
unless 75 > 100 # unless runs code if false
  puts "75 > 100"
end
if 50 == 50
  puts "50 == 50"
end
# if true # This will always be run
#   puts "true"
#   puts "additional code here"
# end
# if false # This will never be run
#   puts "false"
#   puts "additional code here"
# end
#
#
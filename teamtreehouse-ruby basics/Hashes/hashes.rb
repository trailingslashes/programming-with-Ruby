item = Hash.new
item = { "item" => "Bread", "quantity" => 1 }
item = { :item => "Bread", :quantity => 1 }
# add new hash
empty_hash = {}

item[:item] = "Milk"

puts item.hash
puts item.keys

# hash.has_key?("brand")      # => true
# hash.member?("quantity")    # => true
# # hash.key?("item")           # => true
# milk = { "item" => "Milk", "quantity" => 1, "brand" => "Treehouse Dairy" }
#
# puts milk == hash     # => true
#
# bread = { "item" => "Bread", "quantity" => 1, "brand" => "Treehouse Bread Company" }
#
# puts hash == bread     # => false
# grocery_item = { "item" => "Bread", "quantity" => 1, "brand" => "Treehouse Bread Company" }
# if grocery_item.has_value?("Bread")
#   grocery_item.store("food", true)
# end
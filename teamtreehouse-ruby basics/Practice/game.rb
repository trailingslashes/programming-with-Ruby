verb = gets.chomp("Enter a verb: ")
noun = gets.chomp("Enter a noun: ")
adj = gets.chomp("Enter a adjective: ")
noun2 = gets.chomp("Enter a second noun: ")

puts "One day, I decided to learn to #{verb} in Ruby."
puts "So I turned on my #{noun} and logged in to Treehouse."
puts "Their teachers were really #{adj}."
puts "In no time, I'd learned to program a simple #{noun2}!"
def add(num1, num2)
  return num1 + num2
end

grocery_list = %w(milk eggs bread) # inspect prints to one line
grocery_list << 'carrots' # << adds new item to array
grocery_list.push("potatoes")
grocery_list.unshift("celery") # unshift adds new item to the beginning of the array
grocery_list += ["ice cream"] # another way of adding new item to array
# you can also add numbers

puts grocery_list.inspect
puts grocery_list[3]
puts grocery_list.at(0)
puts grocery_list.count
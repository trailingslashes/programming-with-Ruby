system "cls"

def spacer
  puts '-------'
end

#
# lorem_array = %w(Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Erat nam at lectus urna duis. Lorem donec massa sapien faucibus. Mauris commodo quis imperdiet massa tincidunt nunc. Mi proin sed libero enim sed faucibus turpis. Purus ut faucibus pulvinar elementum integer enim. Pellentesque elit ullamcorper dignissim cras tincidunt lobortis feugiat. Eu ultrices vitae auctor eu augue ut lectus arcu bibendum. Viverra nam libero justo laoreet. Nibh tellus molestie nunc non blandit massa. Nec feugiat in fermentum posuere urna. Eu augue ut lectus arcu bibendum. Integer eget aliquet nibh praesent. Habitasse platea dictumst quisque sagittis purus sit. Non arcu risus quis varius quam quisque id diam vel. Mattis ullamcorper velit sed ullamcorper morbi tincidunt. Est ante in nibh mauris cursus mattis molestie a iaculis.)
#
#  unless !lorem_array.empty? # ! - run only of array is empty
#    puts lorem_array
#  end
#
# if !lorem_array.empty?
#   puts lorem_array
# end

sun = ['visible', 'hidden'].sample

# if sun == 'visible'
#   puts "The sun is so bright!"
#   unless sun == 'visible'
#     puts puts "The clouds are blocking the sun!"
#   end
# end

#
# unless sun == 'hidden'
#   puts "The clouds are blocking the sun!"
# end
#


puts 'The sun is so bright!' if sun == 'visible'
puts 'The clouds are blocking the sun!' unless sun == 'visible'

boolean = [true, false].sample
if boolean == true
  puts "im true"
end
boolean ? puts("I'm true!") : puts("I'm false!")
spacer
stoplight = ['green', 'yellow', 'red'].sample
# could also use when

case
  stoplight
  when "green"
    puts "Go"
  when "yellow"
    puts 'puts Slow down'
  when "red"
    puts 'stop!'
  else
    puts nil
end
# else #when 'red'
# puts 'Stop!'
# end
spacer
if stoplight == 'green'
  puts "go"
  elseif stoplight == 'yellow'
  puts "slow down"
else
  puts "Stop!"
end

spacer

status = ['awake', 'tired'].sample

alert = if status == 'awake'
          puts "be productive!"
        else
          puts "go to sleep"
        end

puts alert

spacer

number = rand(10)

if number == 5
  puts '5 is a cool number!'
else
  puts 'Other numbers are cool too!'
end

spacer

stoplight = ['green', 'yellow', 'red'].sample

case stoplight
  when 'green' then puts 'Go!'
  when 'yellow' then puts 'Slow down!'
  else puts 'Stop!'
end

# case stoplight
#   when 'green'  then puts 'Go!'
#   when 'yellow' then puts 'Slow down!'
#   else               puts 'Stop!'
# end
# Proc objects are blocks of code that have been bound to a set
# of local variables. Once bound, the code may be called in
# different contexts and still access those variables.
# Procs are objects
# http://awaxman11.github.io/blog/2013/08/05/what-is-the-difference-between-a-block/




artist = Proc.new do |name, guitar|
  name + " " + guitar.upcase
end

puts artist.call("Eric Clapton", "Stratocaster")
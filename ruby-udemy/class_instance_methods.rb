class User

  # the .self is used to create a method in a class
  def self.all_users
    "All users method"
  end


  # instance methods
  def profile
    "profile method"
  end

  def posts
    "posts method"
  end


  def account
    @account = Account.all
  end

end

# printing a method within a class
p User.all_users


user = User.new
p user.account
p user.posts
p user.profile
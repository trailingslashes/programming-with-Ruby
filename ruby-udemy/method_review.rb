
@players = true
@style = true


def guitar_players
  return ["Eric Clapton", "Jeff Beck", "Mark Knopfler", "Richard Thompson"] if @players == true
  return ["Blues", "Rock"] if @style == true
end


puts guitar_players
def car(price, color, year)
  puts price
  puts color
  puts year
end


car(100.000, blue, 1990)


def post(title:, body:, author:)
  puts title
  puts body
  puts author
end

post(title: "Ruby", body: "A greak book", author: "Some guy")


#default arguments
def post(title:, body: "This was blank", author:)
  puts title
  puts body
  puts author
end

post(title: "Ruby", body: "A greak book", author: "Some guy")

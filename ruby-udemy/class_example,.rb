class MyClass
# @@name = "Cole" # use @@ to define a var in a class
end
ALLCAPS = "www.facebook.com" # constant variable, never changes
$variable_name = "some text" # global var
# @name - instance variable


class OtherClass < MyClass

end

class ThirdClass
# puts @@name.inspect
puts $variable_name
end
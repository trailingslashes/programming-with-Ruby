# Lambda calls
# A lambda is also commonly referred to as an anonymous function.


artist = lambda { |name, guitar| name + " " + guitar }

p artist.call("eric clapton", "cool guitar")

artist_b = ->(name, guitar) {name + " " + guitar}

p artist_b.call("Cole Phillips", "Boss")
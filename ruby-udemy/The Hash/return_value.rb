a_hash =
    {name: "Steve",
     age: 44,
     last_name: "Phillips"
    }

# return a value from hash
p a_hash[:age]

# delete from hash
places = {
  new_york: "Niagara Falls",
  floria: "Jacksonville",
  texas: "Dallas"
  }

p places[:floria]
p places.delete(:floria)
p places
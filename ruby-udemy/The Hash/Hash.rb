
# A has is {}


# most popular format
a_hash =
    {name: "Steve",
     age: 44,
     last_name: "Phillips"
    }

p a_hash
# Hash rocket - :age=>44


hash_rocket = {
    "name" => "John",
    "last_name" => "Doe"
}

p hash_rocket

hash_symbol = {
    :name => "Jimmy",
    :last => "Babaflohi"
}

p hash_symbol
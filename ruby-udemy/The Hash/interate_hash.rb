system "cls"

stuff = {
    name: "Steve",
    age: 44,
    last_name: "Phillips",
    blah: "something",
    more_blah: "blah",
    blah_number: 456
}

stuff.each do |key, value|
  puts key
  puts value
end

puts '-------------'

stuff.each_key do |key|
  p key
end

stuff.each_value do |value|
  p value
end
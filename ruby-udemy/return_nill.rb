def return_nil
  puts "Hello Ruby" # just printing something to the console does not mean there's a value there
end

def return_not_nil
  puts "Hello Ruby! not nil"
end


p return_nil
p return_not_nil

is_nil = return_nil
not_nil = return_not_nil

p is_nil
p not_nil
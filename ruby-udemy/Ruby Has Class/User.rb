# 1. create a user class
# 2. a user should have first name, last name, email
# create a method to puts first last email
# create a subclass that is for admin and puts out "welcome to the admin panel"
# create a subclass paid user needs to output paid section


class User
  system "cls"
  attr_accessor :first, :last, :email

  def initialize(first: first_name, last: last_name, email: email)
    @first = first
    @last = last
    @email = email
  end

  def user_data
    puts @first
    puts @last
    puts @email
  end
end

class AdminUser < User
  def admin_welcome_message
    puts "welcome to the admin panel"
  end

end

class PaidUser < User
  def paid_welcome_message
    puts "welcome to the paid user section"
  end
end

admin = AdminUser.new(first: "Admin", last: "Phillips",
                email: "colephillips1@gmail.com")



admin.user_data
admin.admin_welcome_message
paid.user_data

class Car
  attr_accessor :make, :model, :year, :age # takes care of the getter and setter
#  attr_reader # getter
#  attr_writer # setter

  def initialize(make: make, model: model, year: year, age: age)
    @make = make
    @model = model
    @year = year
    @age = age
  end

  def print_it
    puts @make
    puts @model
    puts @year
    puts @age
  end

  def get_the_year
    @current_time = Time.now.strftime("%-m/%-d/%Y").to_f
    @car_year_neg = @year - @current_time
    @car_year = @car_year_neg * -1
    puts @car_year
  end

end


new_car = Car.new(make: "Ford", model: "Focus", year: 2010)
new_car.print_it # the .make turns the var into a string
new_car.get_the_year
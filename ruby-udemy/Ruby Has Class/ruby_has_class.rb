
# 1. create the class
# 2. initialize the class
# 3. create getter method
# create setter method


class Car
  attr_accessor :make # takes care of the getter and setter
#  attr_reader # getter
#  attr_writer # setter

  def initialize(make)
    @make = make
  end

end


new_car = Car.new("Tesla")
p new_car.make # the .make turns the var into a string

new_car.make = "Toyota"
p new_car.make # setting it
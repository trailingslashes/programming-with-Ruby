


# # does not display the error, puts out an error message
#
# begin
#   puts 'abc' / 'xyz'
# rescue
#   puts "There was an error!"
# end


# puts 0 + nil TypeError
# puts 'asdf' / 'asds' NoMethodError


begin
 puts 'asd' / 'asdsd'
rescue NoMethodError => e # could be anything, after rescue, you could use StandardError, catch all for everything
  puts "This is an error message of type:  #{e}"
end
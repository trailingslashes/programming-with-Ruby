# unlimited arugments
def carlist(*cars)
  puts cars
end


carlist("Tesla", "Ford", "GMC")

# ** captures all keyword arguments
def used_cars(**used_list)
  used_list.each do |make, year|
    puts "Make: #{make}"
    puts "Year: #{year}"
  end
end

this_is_data = {
    "Toyota": "1998",
    "GMC": "2010",
    "Tesla": "2018",
    "Dodge": "1999"
}

used_cars this_is_data

def sold_cars(options={})
  puts options[:make]
  puts options[:model]
  puts options[:year]
end

sold_cars(make: "Ford", model: "A12", year: "2018")
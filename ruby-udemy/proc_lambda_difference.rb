# Summary differences



# - Lambdas check the numbers of arguments, while procs do not

lambda_a = lambda { |word_a, word_b | word_a + " " + word_b }
p lambda_a.call("Lambda", "getting called")

lambda_b = lambda { |word_a, word_b | word_a + " " + word_b }
p lambda_b.call("Lambda", "getting called")

proc_a = Proc.new {|x, y| x + " " + y}
p proc_a.call("proc", "2 args", "3 getting called")

proc_b = Proc.new {|x| x}
p proc_b.call("this is x", "2 args", "3 getting called")

# - Lambdas and procs treat the 'return keyword differently'

def lambda_return
  this_is_a_lambda = lambda { return "This is a lambda string"}
  this_is_a_lambda.call

  this_is_a_lambda_2 = lambda { |arg| arg}
  this_is_a_lambda_2.call("arg getting called")

end

p lambda_return

def proc_return
  this_is_a_proc = Proc.new { return "This is a proc string"}
  this_is_a_proc.call

  this_is_a_proc_2 = Proc.new { |arg| arg}
  this_is_a_proc_2.call("proc arg getting called")

end

p proc_return
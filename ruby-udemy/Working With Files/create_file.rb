# r -- read-only
# r+ -- read and write, update the file
# w -- write-only (if the file exists, overwrites)
# w+ -- read and write (if the file exists, overwrites)
# a -- append file (add to file)
# a+ -- read-write(if the file exists
# start at the end of the file. Suitable for updating files)

file = File.new('files/names.txt', 'w+')
file.puts("name 1", 'some other info', 'Hello World!')

file_emails = File.new('files/emails.txt', 'w+')
file_emails.puts("john@mail.com", 'cole@mail.com', 'sally@mail.com')

file.close
file_emails.close


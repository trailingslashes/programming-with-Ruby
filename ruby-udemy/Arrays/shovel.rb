some_var = "Ruby"

some_array = ["R", "U", "B", "Y"]


shovel_array = some_array << "hello", "Some name", 565, (5 + 5)
# puts some_array
puts '-----'

puts shovel_array
puts '============'

# array delete methods
# delete
# delete_at
# delete_if


array = [1289,'name', 'hello']
array_num = [1289,1100, 800, 500, 600]
mod_array = array.delete('name')
mod_array = array.delete_at(3)
mod_array_delete_if = array_num.delete_if {|less| less < 1000}
p array
p mod_array_delete_if


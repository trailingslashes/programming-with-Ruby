string = 'I have a new red jacket and it looks great with my red car'

puts string.sub("red", "green") # find and replace first instance
 puts string.gsub!("red", "green") # find and replace globally
puts string # add a bang ! to save from first output

# I have a new green jacket and it looks great with my red car
# I have a new green jacket and it looks great with my green car
# I have a new green jacket and it looks great with my green car
# last line output has a bang !
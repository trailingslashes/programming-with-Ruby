def encrypt(message)
 arry = message.split("").map do |x|
   ord_var = x.ord + 1
   ord_var.chr
 end
 return arry.join("")
end

def decrypt(message)
 arry = message.split("").map do |x|
   ord_var = x.ord - 1
   ord_var.chr
 end
 return arry.join("")
end

encrypted_message = encrypt("hello")
puts encrypted_message
puts decrypt(encrypted_message)

for num in 1..100
  if num % 5 == 0 and num % 3 == 0 
    puts "#{num} :FizzBuzz"
  elsif
    num % 3 == 0
    puts "#{num} :Buzz"
  elsif
    num % 5 == 0
    puts "#{num} :Fizz"
  else
    puts "#{num} :Not divisible by 5 or 3"
  end
end
